Categories:Internet
License:GPL-3.0
Web Site:https://tom79.bitbucket.io/
Source Code:https://bitbucket.org/tom79/mastodon_etalab/src
Issue Tracker:https://bitbucket.org/tom79/mastodon_etalab/issues

Auto Name:Mastalab
Summary:Multi-account client for Mastodon
Description:
Mastalab allows you to manage several accounts and to switch from one to another
by a simple click. You can start to compose messages and finish them later by a
mechanism of auto-recording, and these messages can be scheduled. Stay aware due
to a system of push notifications when new content is available or when
receiving a notification on an account. Numerous features and settings
parameters will allow to tailor the behavior of the app with what you need.
.

Repo Type:git
Repo:https://bitbucket.org/tom79/mastodon_etalab.git

Build:1.3.7,34
    commit=1.3.7
    subdir=app
    gradle=yes

Build:1.3.8.1,37
    commit=1.3.8.1
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.3.8.1
Current Version Code:37
